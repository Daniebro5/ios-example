//
//  SecondaryViewController.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Action
import UIKit
import RxSwift

class SecondaryViewController: UIViewController {

  private var logoutbutton = UIButton()

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .brown

    view.addSubview(logoutbutton)

    logoutbutton.snp.makeConstraints {
      $0.center.equalToSuperview()
    }
    // Do any additional setup after loading the view.

    logoutbutton.rx.action = logoutAction
  }


  lazy var logoutAction = CocoaAction { _ in
    userStatus.accept(.loggedOut)
    return Observable.empty()
  }
  /*
   // MARK: - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destination.
   // Pass the selected object to the new view controller.
   }
   */
}
