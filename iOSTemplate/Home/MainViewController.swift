//
//  MainViewController.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Action
import RxSwift
import UIKit
import XCoordinator

class MainViewController: UIViewController {

  var continueButton = UIButton()
  var pageView = UIView()
  var router: StrongRouter<PageRoute>

  init(router: StrongRouter<PageRoute> = HomePageCoordinator().strongRouter) {
    self.router = router
    super.init(nibName: nil, bundle: nil)
    continueButton.setTitle("Continue", for: .normal)

    view.addSubview(pageView)
    view.addSubview(continueButton)
    pageView.snp.makeConstraints {
      $0.leading.top.trailing.equalTo(view.layoutMargins)
      $0.bottom.equalTo(continueButton.snp.top).offset(-44)
    }

    continueButton.snp.makeConstraints {
      $0.centerX.equalToSuperview()
      $0.bottom.equalTo(view.snp_bottomMargin).offset(-44)
    }

    continueButton.rx.action = continueAction
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .cyan
  }

  lazy var continueAction = CocoaAction { _ in
    return self.router.rx.trigger(.next)
  }
}
