//
//  HomeCoordinator.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Foundation

import UIKit
import XCoordinator

enum HomeRoute: Route {
  case main
  case secondary
  case pages
}

final class HomeCoordinator: TabBarCoordinator<HomeRoute> {
  let vc1: MainViewController
  let vc2 = SecondaryViewController()
  let pageCoord: StrongRouter<PageRoute>

  init() {
    pageCoord = HomePageCoordinator().strongRouter
    vc1 = MainViewController(router: pageCoord)
    vc1.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 0)
    vc2.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 1)
    super.init(tabs: [vc1, vc2], select: vc1)
    self.trigger(.pages)
  }

  // MARK: Overrides
  override func prepareTransition(for route: HomeRoute) -> TabBarTransition {
    switch route {
    case .main:
      return .select(vc1)
    case .secondary:
      return .select(vc2)
    case .pages:
      return .embed(pageCoord, in: vc1.pageView)
    }
  }
}
