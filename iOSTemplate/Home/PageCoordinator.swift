//
//  PageCoordinator.swift
//  iOSTemplate
//
//  Created by Danni Brito on 6/1/21.
//

import UIKit
import XCoordinator
import RxSwift

enum PageRoute: Route {
  case next
}

final class HomePageCoordinator: PageCoordinator<PageRoute> {

  // MARK: Stored properties

  private let views = [SecondaryViewController(), SecondaryViewController()]

  private var current: Int

  // MARK: Initialization

  init() {
    current = 0
    super.init(
      rootViewController: .init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil),
      pages: views,
      loop: false,
      direction: .forward
    )

    Observable<Int>
      .timer(.seconds(3), scheduler: MainScheduler.instance)
      .take(1)
      .subscribe(onNext: { _ in
        self.trigger(.next)
      })
  }

  // MARK: Overrides

  override func prepareTransition(for route: PageRoute) -> PageTransition {
    switch route {
      case .next:
        return .set(views[1], direction: .reverse)
    }
  }
}
