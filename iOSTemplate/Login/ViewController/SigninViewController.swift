//
//  SigninViewController.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Action
import UIKit
import RxSwift
import XCoordinator

class SigninViewController: UIViewController {

  private var loginButton = UIButton()
  private let router: StrongRouter<LoginRoute>

  init(router: StrongRouter<LoginRoute>) {
    self.router = router
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }


  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .red
    // Do any additional setup after loading the view.
    view.addSubview(loginButton)
    loginButton.snp.makeConstraints {
      $0.center.equalToSuperview()
    }

    loginButton.setTitle("click to login", for: .normal)
    loginButton.rx.action = homeAction
  }


  lazy var homeAction = CocoaAction { _ in
    userStatus.accept(.loggedIn)
    return self.router.rx.trigger(.home)
  }
}
