//
//  WelcomeViewController.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import SnapKit
import UIKit
import Action
import XCoordinator

class WelcomeViewController: UIViewController {

  private var mainStack = UIStackView()
  private var signup = UIButton()
  private var signin = UIButton()

  private let router: StrongRouter<LoginRoute>

  init(router: StrongRouter<LoginRoute>) {
    self.router = router
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .blue
    // Do any additional setup after loading the view.

    signup.setTitle("SignUp", for: .normal)
    signin.setTitle("signin", for: .normal)


    signup.rx.action = signupAction
    signin.rx.action = signinAction

    view.addSubview(mainStack)
    mainStack.addArrangedSubview(signup)
    mainStack.addArrangedSubview(signin)

    mainStack.snp.makeConstraints {
      $0.center.equalToSuperview()
    }
  }

  lazy var signupAction = CocoaAction { _ in
    return self.router.rx.trigger(.signup)
  }

  lazy var signinAction = CocoaAction { _ in
    return self.router.rx.trigger(.signin)
  }
}
