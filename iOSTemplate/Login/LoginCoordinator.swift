//
//  LoginCoordinator.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import UIKit
import XCoordinator

enum LoginRoute: Route {
  case welcome
  case signup
  case signin
  case home
}

final class LoginCoordinator: NavigationCoordinator<LoginRoute> {

  init(initialRoute: LoginRoute? = nil) {
    super.init(initialRoute: initialRoute)
  }

  // MARK: Overrides
  override func prepareTransition(for route: LoginRoute) -> NavigationTransition {
    switch route {
      case .welcome:
        let vc = WelcomeViewController(router: strongRouter)
        return .push(vc)
      case .signup:
        let vc = SignupViewController()
        return .push(vc)
      case .signin:
        let vc = SigninViewController(router: strongRouter)
        return .push(vc)
      case .home:
        return .pop()
    }
  }
}
