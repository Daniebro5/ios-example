//
//  Constants.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Foundation

enum LoginStatus: String {
  case loggedOut
  case loggedIn
}
