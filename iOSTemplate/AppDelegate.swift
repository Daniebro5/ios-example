//
//  AppDelegate.swift
//  iOSTemplate
//
//  Created by Danni Brito on 5/22/21.
//

import Action
import UIKit
import RxRelay
import RxSwift
import XCoordinator

let userStatus: PublishRelay<LoginStatus> = PublishRelay()

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  // MARK: Stored properties

  private lazy var mainWindow = UIWindow()
  private let disposeBag = DisposeBag()
  private lazy var loginRouter = LoginCoordinator(initialRoute: .welcome).strongRouter
  private lazy var homeRouter = HomeCoordinator().strongRouter

  // MARK: UIApplicationDelegate

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    configureUI()


    let subscription = userStatus.share()
    subscription
      .first()
      .subscribe(onSuccess: { [weak self] status in
        guard let self = self else { return }
        if status == .loggedIn {
          self.homeRouter.setRoot(for: self.mainWindow)
        } else {
          self.loginRouter.setRoot(for: self.mainWindow)
        }
      })
      .disposed(by: disposeBag)

    subscription
      .skip(1)
      .distinctUntilChanged()
      .bind(to: handleLoggedInStatusAction.inputs)
      .disposed(by: disposeBag)

    loadUserSininStatus()

    return true
  }

  // MARK: Helpers

  private func configureUI() {
    UIView.appearance().overrideUserInterfaceStyle = .light
  }

  private func loadUserSininStatus() {
    if !UserDefaults.standard.bool(forKey: "isLoggeedIn") {
      userStatus.accept(.loggedIn)
    } else {
      userStatus.accept(.loggedOut)
    }
  }

  private lazy var handleLoggedInStatusAction: Action<LoginStatus, Void> = Action { newStatus in

    if newStatus == .loggedIn {
      self.homeRouter.setRoot(for: self.mainWindow)
    } else {
      self.loginRouter.setRoot(for: self.mainWindow)
    }
    return Observable.empty()
  }
}
